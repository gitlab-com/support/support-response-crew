# Support Response Crew

This Project has been created with the issue [Improve Support Response Crew scheduling/rotation](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/2886) in mind.
For the time being it serves as a Proof of Concept. Please read the issue for more information.